#!/usr/bin/env python

import os

from flask import Flask, make_response, request

app = Flask('do_webhook')

with file('/var/www/gitlab-key', 'r') as f:
    allowed_apikey = f.read().strip()

@app.route('/', methods=['POST'])
def index():
    apikey = request.headers['X-Gitlab-Token']
    if apikey != allowed_apikey:
        return make_response('bad key', 401)

    if request.json is None:
        return make_response('request not json', 400)

    if request.json['object_kind'] != 'push':
        return make_response('ignoring non-push', 400)

    updated_repo = request.json['project']['path_with_namespace']
    updated_repo = os.path.basename(updated_repo)
    filename = os.path.join(app.root_path, 'updates', updated_repo)
    #with file(filename, 'w') as f:
    #    f.write(request.json['after'])

    return make_response('ok', 200)
